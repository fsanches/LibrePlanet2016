INKSCAPE = inkscape
GHOSTSCRIPT = gs

SLIDES := $(patsubst %.svg,%.pdf,$(wildcard *.svg))

PDF = LibrePlanet_2016_FSanches.pdf

all: $(PDF)

clean:
	rm -rf $(SLIDES)
	rm $(PDF)

slide%.pdf: slide%.svg
	$(INKSCAPE) -f $< --export-pdf=$@

$(PDF): $(SLIDES)
	$(GHOSTSCRIPT) -q -sPAPERSIZE=letter -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$(PDF) $(SLIDES)
	evince $(PDF)
